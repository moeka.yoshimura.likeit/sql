﻿SELECT item_id, item_name, item_price, category_name
FROM item t
LEFT JOIN item_category t1
ON t.category_id = t1.category_id;
