﻿SELECT category_name, SUM(item_price) AS total_price
FROM item t
LEFT JOIN item_category t1
ON t.category_id = t1.category_id
GROUP BY category_name
ORDER BY total_price DESC;
